# Creating Workouts and Exercises before you can create Workouts ==> Rounds ==> Exercises

## Add Workouts
`lib/migrations/workouts.ts`

## Add Exercies
`lib/migrations/exercises.ts`

```
Please reload enum values for both of the ENUM TABLES workout_names and exercise_names
```

## Add Workouts all together with Rounds and Exercises
`lib/migrations/[workout-name].ts`


For example:

```
export const <workout-name> = createRound({
  roundsCount: 5,
  workout: Athena,
  rounds: [
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 25,
        },
        {
          ...Situps,
          repetitionsCount: 25,
        },
        {
          ...Squats,
          repetitionsCount: 25,
        },
        {
          ...Rest,
          exerciseSeconds: 25,
        },
      ],
    },
    .....
    .....
```

Don't forget to export in `lib/migrations/workouts_with_rouds/index.ts` 

on the bottom of the page!

```
/**
 * EXPORT AL THE WORKOUTS ==> ROUNDS ==> EXERCISES HERE!
 */
export * from './athena';
export * from './morpheus';
export * from './prometheus';
```


## add to `lib/migrations/create_rounds.ts`

```
export default flatten([athena, morpheus, prometheus, ...<workout-name>, ...<workout-name>]);
```

Finally to set it to the DB run in the Root directory:

```
npm run migration
```