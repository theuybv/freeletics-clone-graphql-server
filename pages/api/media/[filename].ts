import gridFSBucket from "../../../lib/migrations/gridFSBucket";
import { parse } from "path";
const { router, get } = require("microrouter");

export default router(
  get("/api/media/:filename", async (req: any, res: any) => {
    const bucket = await gridFSBucket;
    const { filename } = req.params;

    try {
      const files = await bucket
        .find({
          filename,
        })
        .toArray();
      const file = files[0];

      const ContentType =
        parse(filename).ext === ".mp4" ? "video/mp4" : "image/jpg";
    
      const head = {
        "Content-Length": file.length,
        "Content-Type": ContentType,
      };
      res.writeHead(200, head);
      bucket.openDownloadStreamByName(filename).pipe(res);
    } catch (e) {
      res.status(400).json({ message: `file: ${filename} does not exist` });
    }
  })
);
