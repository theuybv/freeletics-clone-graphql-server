import { ApolloServer } from "apollo-server-micro";
import { schema } from "../../lib/schema";
const { router, get, post, options } = require("microrouter");

const apolloServer = new ApolloServer({ schema });

export const config = {
  api: {
    bodyParser: false,
  },
};

const graphqlPath = "/api/graphql";
const graphqlHandler = apolloServer.createHandler({ path: graphqlPath });

export default router(
  post(graphqlPath, graphqlHandler),
  get(graphqlPath, graphqlHandler),
);
