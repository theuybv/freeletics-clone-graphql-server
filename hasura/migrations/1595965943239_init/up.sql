CREATE TABLE public.active_workouts (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "elapsedSeconds" integer DEFAULT 0 NOT NULL,
    "precentageOfCompletion" numeric DEFAULT 0 NOT NULL,
    "currentExcerciseName" text,
    "currentRound" integer DEFAULT 1 NOT NULL,
    "startTime" timestamp with time zone DEFAULT now() NOT NULL,
    "endTime" timestamp with time zone,
    completed boolean DEFAULT false NOT NULL,
    "quitAtExerciseName" text,
    "quitAtRound" integer,
    "workoutName" text NOT NULL
);
CREATE TABLE public.exercise_names (
    name text NOT NULL
);
CREATE TABLE public.exercises (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    movie text,
    thumb text,
    name text NOT NULL
);
CREATE TABLE public.rounds (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    "workoutName" text NOT NULL,
    number integer NOT NULL,
    "repetitionsCount" integer,
    "exerciseSeconds" integer,
    "exerciseName" text
);
CREATE TABLE public.sqlgrid_chunks (
    id bigint NOT NULL,
    sha256 text,
    data bytea
);
CREATE SEQUENCE public.sqlgrid_chunks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.sqlgrid_chunks_id_seq OWNED BY public.sqlgrid_chunks.id;
CREATE TABLE public.sqlgrid_files (
    id bigint NOT NULL,
    filename text,
    sha256 text,
    size bigint,
    finished_at timestamp with time zone,
    status text
);
CREATE SEQUENCE public.sqlgrid_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.sqlgrid_files_id_seq OWNED BY public.sqlgrid_files.id;
CREATE TABLE public.sqlgrid_pointers (
    id bigint NOT NULL,
    num bigint,
    file_id bigint,
    chunk_id bigint
);
CREATE SEQUENCE public.sqlgrid_pointers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.sqlgrid_pointers_id_seq OWNED BY public.sqlgrid_pointers.id;
CREATE TABLE public.workout_names (
    name text NOT NULL
);
CREATE TABLE public.workouts (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    name text NOT NULL,
    difficulty integer,
    targets text,
    duration integer
);
ALTER TABLE ONLY public.sqlgrid_chunks ALTER COLUMN id SET DEFAULT nextval('public.sqlgrid_chunks_id_seq'::regclass);
ALTER TABLE ONLY public.sqlgrid_files ALTER COLUMN id SET DEFAULT nextval('public.sqlgrid_files_id_seq'::regclass);
ALTER TABLE ONLY public.sqlgrid_pointers ALTER COLUMN id SET DEFAULT nextval('public.sqlgrid_pointers_id_seq'::regclass);
ALTER TABLE ONLY public.active_workouts
    ADD CONSTRAINT active_workouts_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.exercise_names
    ADD CONSTRAINT exercise_names_pkey PRIMARY KEY (name);
ALTER TABLE ONLY public.exercises
    ADD CONSTRAINT exercises_pkey PRIMARY KEY (name);
ALTER TABLE ONLY public.rounds
    ADD CONSTRAINT rounds_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.rounds
    ADD CONSTRAINT "rounds_workoutName_exerciseName_number_key" UNIQUE ("workoutName", "exerciseName", number);
ALTER TABLE ONLY public.sqlgrid_chunks
    ADD CONSTRAINT sqlgrid_chunks_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.sqlgrid_files
    ADD CONSTRAINT sqlgrid_files_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.sqlgrid_pointers
    ADD CONSTRAINT sqlgrid_pointers_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.workout_names
    ADD CONSTRAINT workout_names_pkey PRIMARY KEY (name);
ALTER TABLE ONLY public.workouts
    ADD CONSTRAINT workouts_pkey PRIMARY KEY (name);
CREATE UNIQUE INDEX sqlgrid_chunks_sha256 ON public.sqlgrid_chunks USING btree (sha256);
CREATE INDEX sqlgrid_files_status_filename_finished_at ON public.sqlgrid_files USING btree (status, filename, finished_at);
CREATE INDEX sqlgrid_pointers_chunk_id ON public.sqlgrid_pointers USING btree (chunk_id);
CREATE UNIQUE INDEX sqlgrid_pointers_file_id_num ON public.sqlgrid_pointers USING btree (file_id, num);
ALTER TABLE ONLY public.active_workouts
    ADD CONSTRAINT "active_workouts_currentExcerciseName_fkey" FOREIGN KEY ("currentExcerciseName") REFERENCES public.exercises(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.active_workouts
    ADD CONSTRAINT "active_workouts_quitAtExerciseName_fkey" FOREIGN KEY ("quitAtExerciseName") REFERENCES public.exercises(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.active_workouts
    ADD CONSTRAINT "active_workouts_workoutName_fkey" FOREIGN KEY ("workoutName") REFERENCES public.workouts(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.exercises
    ADD CONSTRAINT exercises_name_fkey FOREIGN KEY (name) REFERENCES public.exercise_names(name) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.rounds
    ADD CONSTRAINT "rounds_exerciseName_fkey" FOREIGN KEY ("exerciseName") REFERENCES public.exercises(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.rounds
    ADD CONSTRAINT "rounds_workoutName_fkey" FOREIGN KEY ("workoutName") REFERENCES public.workouts(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.sqlgrid_pointers
    ADD CONSTRAINT sqlgrid_pointers_chunk_id_fkey FOREIGN KEY (chunk_id) REFERENCES public.sqlgrid_chunks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.sqlgrid_pointers
    ADD CONSTRAINT sqlgrid_pointers_file_id_fkey FOREIGN KEY (file_id) REFERENCES public.sqlgrid_files(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.workouts
    ADD CONSTRAINT workouts_name_fkey FOREIGN KEY (name) REFERENCES public.workout_names(name) ON UPDATE CASCADE ON DELETE CASCADE;
