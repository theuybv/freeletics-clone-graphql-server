import { resolve, parse } from "path";
const recursive = require("recursive-readdir");
import { snakeCase } from "lodash";
import { writeFileSync } from "fs";

(async () => {
  const filePaths: string[] = await recursive(
    resolve(__dirname, "data/media/thumbs")
  );

  const data = filePaths.map((filePath) => {
    /**
     * Snakecase is needed because postgres ENUMS can not handle spaces or specia charachters
     */
    const name = snakeCase(parse(filePath).name);
    return {
      name,
    };
  });

  writeFileSync(
    resolve(__dirname, "data", "data.json"),
    JSON.stringify(data, null, 4)
  );
})();
