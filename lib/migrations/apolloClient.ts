import { ApolloClient } from "apollo-client";
import fetch from "node-fetch";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

const apolloClient = new ApolloClient({
  link: new HttpLink({
    uri: process.env.HASURA_GRAPHQL_ENDPOINT || "http://localhost:8080/v1/graphql",
    fetch: fetch as any,
  }),
  cache: new InMemoryCache(),
});

export default apolloClient;
