import apolloClient from "./apolloClient";
import gql from "graphql-tag";
import gridFSBucket from "./gridFSBucket";

export default () => {
  return new Promise(async (_resolve, _reject) => {
    try {
      const bucket = await gridFSBucket;
      await bucket.drop();
    } catch (e) {}

    const result = await apolloClient.mutate({
      mutation: gql`
        mutation delete_all {
          delete_workout_names(where: {}) {
            affected_rows
          }
          delete_exercise_names(where: {}) {
            affected_rows
          }
          delete_rounds(where: {}) {
            affected_rows
          }
          delete_workouts(where: {}) {
            affected_rows
          }
          delete_exercises(where: {}) {
            affected_rows
          }
          delete_active_workouts(where: {}) {
            affected_rows
          }
        }
      `,
    });

    _resolve(result);
  });
};
