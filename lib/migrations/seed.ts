import delete_all from "./delete_all";
// import insert_media_files from "./insert_media_files";
import insert_workout_names from "./insert_workout_names";
import insert_exercise_names from "./insert_exercise_names";
import insert_rounds from "./insert_rounds";

(async () => {
  try {
    await delete_all();
    await insert_workout_names();
    await insert_exercise_names();
    // await insert_media_files();
    await insert_rounds();
    console.log("SEEDING APPLICATION DATA IS DONE!");
    process.exit(0);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})();
