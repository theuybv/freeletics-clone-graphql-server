import gql from "graphql-tag";
import apolloClient from "./apolloClient";
import { flatten, snakeCase } from "lodash";
import { Exercise } from "./exercises";
import workout_rounds, { Round } from "./create_rounds";

const data = flatten(
  workout_rounds.map(
    (
      {
        number: roundNumber,
        workout: { title: workoutTitle, difficulty, duration, targets },
        exercises,
      }: Round    ) => {
      return exercises.map(
        ({
          repetitionsCount,
          exerciseSeconds,
          title: exerciseTitle,
        }: Exercise) => {
          console.log(exerciseTitle);
          console.log(workoutTitle);
          return {
            number: roundNumber,
            repetitionsCount,
            exerciseSeconds,
            workout: {
              on_conflict: {
                constraint: "workouts_pkey",
                update_columns: ["name"],
              },
              data: {
                name: snakeCase(workoutTitle),
                title: workoutTitle,
                difficulty,
                duration,
                targets,
              },
            },
            exercise: {
              on_conflict: {
                constraint: "exercises_pkey",
                update_columns: ["name"],
              },
              data: { name: snakeCase(exerciseTitle), title: exerciseTitle },
            },
          };
        }
      );
    }
  )
);

export default () => {
  return new Promise(async (_resolve, _reject) => {
    const result = await apolloClient.mutate({
      mutation: gql`
        mutation insert_rounds($data: [rounds_insert_input!]!) {
          insert_rounds(
            on_conflict: {
              constraint: rounds_workoutName_exerciseName_number_key
              update_columns: [
                number
                workoutName
                exerciseName
                repetitionsCount
                exerciseSeconds
              ]
            }
            objects: $data
          ) {
            affected_rows
          }
        }
      `,
      variables: {
        data,
      },
    });
    _resolve(result);
  });
};
