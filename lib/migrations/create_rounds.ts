import { Workout } from "./workouts";
import { Exercise } from "./exercises";
import { flatten } from "lodash";
import {
  morpheus,
  prometheus,
  athena,
  kerberos,
  acrazymeworkout,
} from "./workouts_with_rounds";

export type Round = {
  number: number;
  workout: Workout;
  exercises: Exercise[];
};

export default flatten([
  athena,
  morpheus,
  prometheus,
  kerberos,
  acrazymeworkout,
]);
