import apolloClient from "./apolloClient";
import gql from "graphql-tag";
import data from "./data/data.json";

export default () => {
  return new Promise(async (_resolve, _reject) => {
    await apolloClient.mutate({
      mutation: gql`
        mutation insert_exercise_names($data: [exercise_names_insert_input!]!) {
          insert_exercise_names(objects: $data) {
            affected_rows
          }
        }
      `,
      variables: {
        data: [
          {
            name: "rest",
          },
        ],
      },
    });

    const result = await apolloClient.mutate({
      mutation: gql`
        mutation insert_exercise_names($data: [exercise_names_insert_input!]!) {
          insert_exercise_names(objects: $data) {
            affected_rows
          }
        }
      `,
      variables: {
        data,
      },
    });

    _resolve(result);
  });
};
