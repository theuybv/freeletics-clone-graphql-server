import { createReadStream } from "fs";
import { resolve, parse } from "path";
import { flatten } from "lodash";
import gridFSBucket from "./gridFSBucket";
const recursive = require("recursive-readdir");

const assert = require("assert");

const writeFilesToDb = async (pathToFolder: any) => {
  const filePaths: string[] = await recursive(pathToFolder);
  const bucket = await gridFSBucket;

  return filePaths.map((filePath: string) => {
    return new Promise((resolve, reject) => {
      const filename = parse(filePath).base;
      createReadStream(filePath)
        .pipe(bucket.openUploadStream(filename))
        .on("error", function (error: any) {
          assert.ifError(error);
          reject(error);
        })
        .on("finish", function () {
          console.log("uploaded: " + filename);
          resolve(filePath);
        });
    });
  });
};

export default () => {
  return new Promise(async (_resolve, _reject) => {
    const thumbs = await writeFilesToDb(
      resolve(__dirname, "data/media/thumbs")
    );
    const videos = await writeFilesToDb(
      resolve(__dirname, "data/media/videos")
    );
    const files = await Promise.all(flatten([thumbs, videos]));
    _resolve(files);
  });
};
