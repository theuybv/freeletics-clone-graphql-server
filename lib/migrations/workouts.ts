export type Workout = {
  title: string;
  difficulty: number;
  duration: number;
  targets: string;
};

export const Athena: Workout = {
  title: "Athena",
  difficulty: 1,
  duration: 1,
  targets: "Core, Lower",
};

export const Morpheus: Workout = {
  title: "Morpheus",
  difficulty: 2,
  duration: 1,
  targets: "Full body",
};

export const Prometheus: Workout = {
  title: "Prometheus",
  difficulty: 2,
  duration: 2,
  targets: "Core, Lower",
};

export const Kerberos: Workout = {
  title: "Kerberos",
  difficulty: 3,
  duration: 2,
  targets: "Full Body",
};

export const ACrazyMeWorkout: Workout = {
  title: "AAWorkout",
  difficulty: 3,
  duration: 2,
  targets: "Full Body",
};

/**
 * EXPORT ALL THE WORKOUTS!
 */
export const allWorkouts = [Athena, Morpheus, Prometheus, Kerberos, ACrazyMeWorkout];
