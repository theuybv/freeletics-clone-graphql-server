export type Exercise = {
  title: string;
  repetitionsCount?: number;
  exerciseSeconds?: number;
};

export const Climbers = {
  title: "Climbers",
};

export const Situps = {
  title: "Situps",
};

export const Squats = {
  title: "Squats",
};

export const Rest = {
  title: "Rest",
};

export const Pushups = {
  title: "Pushups",
};

export const Lunges = {
  title: "Lunges",
};

export const JumpingJacks = {
  title: "Jumping Jacks",
};

export const PlankHold = {
  title: "Plank Hold",
};

export const PlankSwitches = {
  title: "Plank Switches",
};
