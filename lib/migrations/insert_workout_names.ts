import apolloClient from "./apolloClient";
import gql from "graphql-tag";
import { snakeCase } from "lodash";
import { allWorkouts } from "./workouts";

export default () => {
  return new Promise(async (_resolve, _reject) => {
    const data = allWorkouts.map(({ title }) => {
      /**
       * Snakecase is needed because postgres ENUMS can not handle spaces or specia charachters
       */
      return {
        name: snakeCase(title),
      };
    });

    const result = await apolloClient.mutate({
      mutation: gql`
        mutation insert_workout_names($data: [workout_names_insert_input!]!) {
          insert_workout_names(objects: $data) {
            affected_rows
          }
        }
      `,
      variables: {
        data,
      },
    });
    _resolve(result);
  });
};
