import { Athena } from "../workouts";
import { Climbers, Situps, Squats, Rest } from "../exercises";
import { createRound } from ".";

export const athena = createRound({
  roundsCount: 5,
  workout: Athena,
  rounds: [
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 25,
        },
        {
          ...Situps,
          repetitionsCount: 25,
        },
        {
          ...Squats,
          repetitionsCount: 25,
        },
        {
          ...Rest,
          exerciseSeconds: 25,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 20,
        },
        {
          ...Situps,
          repetitionsCount: 20,
        },
        {
          ...Squats,
          repetitionsCount: 20,
        },
        {
          ...Rest,
          exerciseSeconds: 20,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 15,
        },
        {
          ...Situps,
          repetitionsCount: 15,
        },
        {
          ...Squats,
          repetitionsCount: 15,
        },
        {
          ...Rest,
          exerciseSeconds: 15,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 10,
        },
        {
          ...Situps,
          repetitionsCount: 10,
        },
        {
          ...Squats,
          repetitionsCount: 10,
        },
        {
          ...Rest,
          exerciseSeconds: 10,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 5,
        },
        {
          ...Situps,
          repetitionsCount: 5,
        },
        {
          ...Squats,
          repetitionsCount: 5,
        },
      ],
    },
  ],
});
