import { ACrazyMeWorkout } from "../workouts";
import {
  Squats,
  PlankHold,
  PlankSwitches,
} from "../exercises";
import { createRound } from ".";

export const acrazymeworkout = createRound({
  roundsCount: 3,
  workout: ACrazyMeWorkout,
  rounds: [
    {
      exercises: [
        {
          ...PlankHold,
          exerciseSeconds: 5,
        },
        {
          ...PlankSwitches,
          repetitionsCount: 10,
        },
        {
          ...Squats,
          repetitionsCount: 10,
        },
      ],
    },
    {
      exercises: [
        {
          ...PlankHold,
          exerciseSeconds: 5
        },
        {
          ...PlankSwitches,
          repetitionsCount: 20,
        },
        {
          ...Squats,
          repetitionsCount: 20,
        },
      ],
    },
    {
      exercises: [
        {
          ...PlankHold,
          exerciseSeconds: 5,
        },
        {
          ...PlankSwitches,
          repetitionsCount: 30,
        },
        {
          ...Squats,
          repetitionsCount: 30,
        },
      ],
    },
  ],
});
