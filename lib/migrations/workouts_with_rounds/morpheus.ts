import { createRound } from ".";
import { Morpheus } from "../workouts";
import { Pushups, Lunges, JumpingJacks } from "../exercises";

export const morpheus = createRound({
  roundsCount: 5,
  workout: Morpheus,
  rounds: [
    {
      exercises: [
        {
          ...Pushups,
          repetitionsCount: 5,
        },
        {
          ...Lunges,
          repetitionsCount: 10,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 20,
        },
      ],
    },
    {
      exercises: [
        {
          ...Pushups,
          repetitionsCount: 7,
        },
        {
          ...Lunges,
          repetitionsCount: 15,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 30,
        },
      ],
    },
    {
      exercises: [
        {
          ...Pushups,
          repetitionsCount: 10,
        },
        {
          ...Lunges,
          repetitionsCount: 20,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 40,
        },
      ],
    },
    {
      exercises: [
        {
          ...Pushups,
          repetitionsCount: 7,
        },
        {
          ...Lunges,
          repetitionsCount: 15,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 30,
        },
      ],
    },
    {
      exercises: [
        {
          ...Pushups,
          repetitionsCount: 5,
        },
        {
          ...Lunges,
          repetitionsCount: 10,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 20,
        },
      ],
    },
  ],
});
