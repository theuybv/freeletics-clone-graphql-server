import { Athena, Kerberos } from "../workouts";
import {
  Climbers,
  Situps,
  Squats,
  Rest,
  PlankHold,
  PlankSwitches,
} from "../exercises";
import { createRound } from ".";

export const kerberos = createRound({
  roundsCount: 3,
  workout: Kerberos,
  rounds: [
    {
      exercises: [
        {
          ...PlankHold,
          exerciseSeconds: 60,
        },
        {
          ...PlankSwitches,
          repetitionsCount: 10,
        },
        {
          ...Squats,
          repetitionsCount: 10,
        },
      ],
    },
    {
      exercises: [
        {
          ...PlankHold,
          exerciseSeconds: 120,
        },
        {
          ...PlankSwitches,
          repetitionsCount: 20,
        },
        {
          ...Squats,
          repetitionsCount: 20,
        },
      ],
    },
    {
      exercises: [
        {
          ...PlankHold,
          exerciseSeconds: 180,
        },
        {
          ...PlankSwitches,
          repetitionsCount: 30,
        },
        {
          ...Squats,
          repetitionsCount: 30,
        },
      ],
    },
  ],
});
