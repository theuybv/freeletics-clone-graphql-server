import { createRound } from ".";
import { Prometheus } from "../workouts";
import {
  Climbers,
  Pushups,
  Situps,
  Squats,
  JumpingJacks,
  Rest,
} from "../exercises";

export const prometheus = createRound({
  roundsCount: 5,
  workout: Prometheus,
  rounds: [
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 30,
        },
        {
          ...Pushups,
          repetitionsCount: 10,
        },
        {
          ...Situps,
          repetitionsCount: 30,
        },
        {
          ...Squats,
          repetitionsCount: 30,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 50,
        },
        {
          ...Rest,
          exerciseSeconds: 30,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 20,
        },
        {
          ...Pushups,
          repetitionsCount: 7,
        },
        {
          ...Situps,
          repetitionsCount: 20,
        },
        {
          ...Squats,
          repetitionsCount: 20,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 50,
        },
        {
          ...Rest,
          exerciseSeconds: 30,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 10,
        },
        {
          ...Pushups,
          repetitionsCount: 5,
        },
        {
          ...Situps,
          repetitionsCount: 10,
        },
        {
          ...Squats,
          repetitionsCount: 10,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 50,
        },
        {
          ...Rest,
          exerciseSeconds: 30,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 20,
        },
        {
          ...Pushups,
          repetitionsCount: 7,
        },
        {
          ...Situps,
          repetitionsCount: 20,
        },
        {
          ...Squats,
          repetitionsCount: 20,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 50,
        },
        {
          ...Rest,
          exerciseSeconds: 30,
        },
      ],
    },
    {
      exercises: [
        {
          ...Climbers,
          repetitionsCount: 30,
        },
        {
          ...Pushups,
          repetitionsCount: 10,
        },
        {
          ...Situps,
          repetitionsCount: 30,
        },
        {
          ...Squats,
          repetitionsCount: 30,
        },
        {
          ...JumpingJacks,
          repetitionsCount: 50,
        },
      ],
    },
  ],
});
