import { Workout } from '../workouts';
import { Exercise } from '../exercises';
import { Round } from '../create_rounds';
import { flatten } from 'lodash';

type RoundFactory = {
  roundsCount: number;
  workout: Workout;
  rounds: { exercises: Exercise[] }[];
};

export const createRound = ({
  rounds,
  roundsCount,
  workout,
}: RoundFactory): Round[] => {
  if (roundsCount !== rounds.length) {
    throw Error(
      "You must return rounds collection with the exact same length as roundsCount of " +
        roundsCount
    );
  }

  return flatten(
    Array.from(Array(roundsCount)).map((item, index) => {
      return {
        number: index + 1,
        workout: workout,
        exercises: rounds[index].exercises,
      };
    })
  );
};

/**
 * EXPORT AL THE WORKOUTS ==> ROUNDS ==> EXERCISES HERE!
 */
export * from './athena';
export * from './morpheus';
export * from './prometheus';
export * from './kerberos';
export * from './acrazymeworkout';