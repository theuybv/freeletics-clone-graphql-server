import * as mongodb from "mongodb";
const MONGO_DB_NAME = process.env.MONGO_DB_NAME || "freeletics-media";
const MONGO_DB_CONNECTION_URI =
  process.env.MONGO_DB_CONNECTION_URI ||
  "mongodb://localhost:27017/" + MONGO_DB_NAME;
const client = new mongodb.MongoClient(MONGO_DB_CONNECTION_URI, {
  useUnifiedTopology: true,
});

export default (async () => {
  await client.connect();
  const db = client.db(MONGO_DB_NAME);

  return new mongodb.GridFSBucket(db);
})();
