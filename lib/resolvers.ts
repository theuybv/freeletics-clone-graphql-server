import { QueryResolvers } from "./type-defs.graphqls";
import { ResolverContext } from "./apollo";
import apolloClient from "./migrations/apolloClient";
import gql from "graphql-tag";

const Query: Required<QueryResolvers<ResolverContext>> = {
  viewer(_parent, _args, _context, _info) {
    return { id: String(1), name: "John Smith", status: "cached" };
  },
  async mediaByExerciseName(_parent, { exerciseName }, _context, _info) {
    const { data } = await apolloClient.query({
      query: gql`
        query {
          exercise_names_by_pk(name: "${exerciseName}") {
            name
          }
        }
      `,
    });
    if (data.exercise_names_by_pk) {
      const pathToFile = `/api/media/${exerciseName}`;
      return {
        thumbLink: pathToFile + ".jpg",
        videoLink: pathToFile + ".mp4",
      };
    } else {
      throw Error("Media file not found!");
    }
  },
};

export default { Query };
